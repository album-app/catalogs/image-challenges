import sys

import bpy
from mathutils import Color

# logging.info(str(sys.argv))
resolution_percentage = int(sys.argv[len(sys.argv) - 4])
in_path = sys.argv[len(sys.argv) - 3]
out_blend_path = sys.argv[len(sys.argv) - 2]
if out_blend_path == "None":
    out_blend_path = None
out_rendering_path = sys.argv[len(sys.argv) - 1]
if out_rendering_path == "None":
    out_rendering_path = None

bpy.ops.wm.open_mainfile(filepath=in_path)
scene = bpy.context.scene

main_collection = bpy.data.collections['Collection']
objs = set()

# assign material
meshes = set(o for o in scene.objects if o.type == 'MESH')
meshes = sorted(meshes, key=lambda obj: obj.name)
obj_count = len(meshes)

for index, obj in enumerate(meshes):
    material = bpy.data.materials.new('material')
    material.use_nodes = True
    material.node_tree.nodes.new(type='ShaderNodeBsdfGlass')
    inp = material.node_tree.nodes['Material Output'].inputs['Surface']
    c = Color()
    c.hsv = float(index) / float(obj_count), 0.8, 0.5
    material.node_tree.nodes["Glass BSDF"].inputs[0].default_value = (c.r, c.g, c.b, 1)
    material.node_tree.nodes["Glass BSDF"].inputs[1].default_value = 0.5
    outp = material.node_tree.nodes['Glass BSDF'].outputs['BSDF']
    material.node_tree.links.new(inp, outp)
    obj.active_material = material

if out_blend_path:
    bpy.ops.wm.save_as_mainfile(filepath=out_blend_path)

# logging.info("out blend: " + out_blend_path)
# logging.info("out rendering: " + out_rendering_path)

scene = bpy.context.scene

main_collection = bpy.data.collections['Collection']

# focus on data collection
objects = bpy.context.scene.objects
for obj in objects:
    obj.select_set(obj.type == "MESH")
bpy.ops.view3d.camera_to_view_selected()

light = bpy.data.objects['Light']
light.data.type = 'SUN'
light.data.energy = 200

# increase clip end to make sure also bigger datasets are visible
cam_ob = bpy.context.scene.camera
cam_ob.data.clip_end = 30000
cam_ob.data.lens = 45

# show camera view
area = next(area for area in bpy.context.screen.areas if area.type == 'VIEW_3D')
area.spaces[0].region_3d.view_perspective = 'CAMERA'

if out_blend_path:
    bpy.ops.wm.save_as_mainfile(filepath=out_blend_path)
# bpy.context.window.workspace = bpy.data.workspaces["Rendering"]

# enable denoising
# bpy.context.view_layer.cycles.denoising_store_passes = True
# bpy.context.scene.use_nodes = True
#
# tree = bpy.context.scene.node_tree # inputs, outputs, nodes, links
# comp = tree.nodes[0]               # result goes into here
# render = tree.nodes[1]             # render layers: images come from here
# denoise_node = tree.nodes.new(type='CompositorNodeDenoise')
# links = tree.links
# link = links.new(denoise_node.outputs[0], comp.inputs[0])
# link = links.new(render.outputs[0], denoise_node.inputs[0]) # Image
# link = links.new(render.outputs['Denoising Normal'], denoise_node.inputs[1]) # Denoising Normal
# link = links.new(render.outputs['Denoising Albedo'], denoise_node.inputs[2]) # Denoising Albedo

# set background color
world = bpy.data.worlds['World']
world.use_nodes = True
bg = world.node_tree.nodes['Background']
bg.inputs[0].default_value[:3] = (0.01, 0.01, 0.01)

# render
bpy.context.scene.render.engine = 'CYCLES'
# bpy.context.scene.cycles.device = 'GPU'
# bpy.context.scene.view_layers['View Layer'].cycles.use_denoising = True
# bpy.context.scene.render.resolution_x = w
# bpy.context.scene.render.resolution_y = h


bpy.context.scene.use_nodes = True

scene = bpy.context.scene
compositor_node_tree = scene.node_tree
compositor_node_tree.nodes.clear()
render_layers_node = compositor_node_tree.nodes.new('CompositorNodeRLayers')
denoise_node = compositor_node_tree.nodes.new('CompositorNodeDenoise')
compositor_node_tree.links.new(render_layers_node.outputs["Image"], denoise_node.inputs["Image"])
composite_node = compositor_node_tree.nodes.new('CompositorNodeComposite')
compositor_node_tree.links.new(denoise_node.outputs["Image"], composite_node.inputs["Image"])


bpy.context.scene.render.resolution_percentage = resolution_percentage
if out_rendering_path:
    bpy.context.scene.render.filepath = out_rendering_path
    bpy.ops.render.render('INVOKE_DEFAULT', write_still=True)

if out_blend_path:
    bpy.ops.wm.save_as_mainfile(filepath=out_blend_path)
