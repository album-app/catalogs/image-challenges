from album.runner.api import setup

# Please import additional modules at the beginning of your method declarations.
# More information: https://docs.album.solutions/en/latest/solution-development/


def run():
    import imageio.v3 as iio
    import numpy as np
    from wgpu.gui.auto import WgpuCanvas, run
    import pygfx as gfx
    from album.runner.api import get_args

    # Get user input parameters
    image_path = get_args().input
    isosurface = get_args().isosurface

    # Set up the canvas and scene
    canvas = WgpuCanvas()
    renderer = gfx.renderers.WgpuRenderer(canvas)
    scene = gfx.Scene()

    # Read the input dataset
    voldata = iio.imread(image_path).astype(np.float32)

    # Set the volume rendering type
    if isosurface:
        # Render volume as isosurface
        geometry = gfx.Geometry(grid=voldata)
        material = gfx.VolumeIsoMaterial(clim=(0, 2000), threshold=1000)
        vol = gfx.Volume(geometry, material)
    else:
        # Render volume using raycasting
        tex = gfx.Texture(voldata, dim=3)
        vol = gfx.Volume(
            gfx.Geometry(grid=tex),
            gfx.VolumeRayMaterial(clim=(0, 2000), map=gfx.cm.cividis, pick_write=True),
        )

    # Add the volume to the scene
    scene.add(vol)

    # Set up the camera to show the scene
    camera = gfx.PerspectiveCamera(70, 16 / 9)
    camera.show_object(scene, view_dir=(-1, -1, -1), up=(0, 0, 1))

    # Enable interactive rendering
    controller = gfx.OrbitController(camera, register_events=renderer)

    def animate():
        renderer.render(scene, camera)
        canvas.request_draw()

    canvas.request_draw(animate)
    run()


setup(
    group="visualization",
    name="render-volume-pygfx",
    version="0.1.0",
    title="Render a volume using Pygfx",
    description="In this solution, volumes can either be rendered as isosurface or using raycasting.",
    solution_creators=["Deborah Schmidt"],
    cite=[{
        "text": "Pygfx",
        "url": "https://pygfx.org/"
    }],
    license="MIT",
    covers=[{
        "description": "Screenshot of Pygfx rendering a volume.",
        "source": "cover.png"
    }],
    album_api_version="0.5.5",
    args=[{
        "name": "input",
        "type": "file",
        "description": "The image that should be displayed in pygfx.",
        "required": False
    }, {
        "name": "isosurface",
        "type": "boolean",
        "default": False,
        "description": "Whether to render the volume using isosurface material instead of using raycasting.",
        "required": False
    }],
    run=run,
    dependencies={'environment_file': """channels:
  - conda-forge
dependencies:
  - python=3.11
  - pygfx=0.5
  - pyside6=6.7
  - imageio=2.35
"""}
)
