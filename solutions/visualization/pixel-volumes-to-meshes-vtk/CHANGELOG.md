# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2024-09-25
Add preserve_topology arg, remove defaults channel

## [0.1.2] - 2023-10-17
Fix nulltype contour value error for labelmaps.

## [0.1.1] - 2023-09-29
Fixed computation for diverse image datatypes, fixed contour value setting

## [0.1.0] - 2023-09-28

