import bdv.spimdata.SpimDataMinimal;
import bdv.spimdata.XmlIoSpimDataMinimal;
import mpicbg.spim.data.SpimDataException;
import bvv.vistools.BvvFunctions;

import java.io.File;

public class Main {
	public static void main(String...args) throws SpimDataException {
	    String[] argsSplit = args[0].split(",");
	    String xmlFilename = argsSplit[0];
	    String inputWorkingDir = argsSplit[1];
		final SpimDataMinimal spimData = new XmlIoSpimDataMinimal().load( xmlFilename );
	    spimData.setBasePath(new File(inputWorkingDir));
        BvvFunctions.show(spimData);
	}
}