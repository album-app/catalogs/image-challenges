import animation3d.main.BatchRaycaster;
import animation3d.main.InteractiveRaycaster;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;

import java.io.File;

public class Main {
	public static void main(String... args) {
		args = args[0].split(",");
		String inputFile = args[0];
		String animationFile = args[1];
		String outFile = args[4];
		int width = Integer.parseInt(args[2]);
		int height = Integer.parseInt(args[3]);

		if(!outFile.equals("None")) {
// 			new ij.ImageJ(ImageJ.NO_SHOW);
			ImagePlus imp = IJ.openImage(inputFile);
			ImagePlus animation = BatchRaycaster.render(imp, animationFile, width, height);
			IJ.save(animation, outFile);
		} else{
			new ij.ImageJ();
			ImagePlus imp = IJ.openImage(inputFile);
			imp.show();
			InteractiveRaycaster cr = new InteractiveRaycaster();
			cr.setup("", imp);
			cr.run(null);
			if(!animationFile.equals("None"))
				cr.openAnimationFile(new File(animationFile));
		}

	}
}