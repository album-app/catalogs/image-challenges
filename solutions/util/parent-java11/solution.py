from album.runner.api import setup

setup(
    group="util",
    name="parent-java11",
    version="0.1.0",
    solution_creators=["Deborah Schmidt"],
    tags=["java", "parent"],
    album_api_version="0.5.5",
    dependencies={"environment_file": """channels:
  - conda-forge
  - defaults
dependencies:
  - python=3.9
  - requests
  - openjdk=11.0.9.1
"""}
)

