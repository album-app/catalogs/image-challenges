from album.runner.api import setup


def install():
    import subprocess
    import shutil
    from album.runner.api import get_app_path, get_package_path

    get_app_path().mkdir(exist_ok=True, parents=True)

    # copy source files into solution app folder
    shutil.copy(get_package_path().joinpath('build.gradle'), get_app_path())
    shutil.copy(get_package_path().joinpath('gradlew'), get_app_path())
    shutil.copy(get_package_path().joinpath('gradlew.bat'), get_app_path())
    shutil.copytree(get_package_path().joinpath('src'), get_app_path().joinpath('src'))
    shutil.copytree(get_package_path().joinpath('gradle'), get_app_path().joinpath('gradle'))

    # compile app
    subprocess.run([(get_gradle_executable()), 'build', '-Dorg.gradle.internal.http.socketTimeout=300000'],
                   cwd=get_app_path(), check=True)


def get_gradle_executable():
    from sys import platform
    from album.runner.api import get_app_path
    if platform == "win32":
        return get_app_path().joinpath('gradlew.bat')
    return get_app_path().joinpath('gradlew')


def run():
    import subprocess
    from album.runner.api import get_app_path
    subprocess.run([get_gradle_executable(), 'run', '-q'], cwd=get_app_path())

setup(
    group="data-management",
    name="bdv-xml-importer",
    version="0.1.1",
    solution_creators=["Deborah Schmidt"],
    title="Generate BDV XML datasets via BigStitcher",
    description="This solution launches the dataset generator from BigStitcher, which can be used to create a BDV XML file for existing datasets or to convert the data into N5 or HDF5 datasets.",
    tags=["bdv", "bvv", "bigstitcher"],
    cite=[{
        "text": "Hörl, D., Rojas Rusak, F., Preusser, F. et al. BigStitcher: reconstructing high-resolution image datasets of cleared and expanded samples. Nat Methods 16, 870–874 (2019).",
        "doi": "https://doi.org/10.1038/s41592-019-0501-0"
    }, {
        "text": "Pietzsch, T., Saalfeld, S., Preibisch, S., & Tomancak, P. (2015). BigDataViewer: visualization and processing for large image data sets. Nature Methods, 12(6), 481–483.",
        "doi": "10.1038/nmeth.3392"
    }],
    album_api_version="0.5.5",
    install=install,
    run=run,
    dependencies={'parent': {
        "resolve_solution": "util:parent-java11:0.1.0"
    }}
)

