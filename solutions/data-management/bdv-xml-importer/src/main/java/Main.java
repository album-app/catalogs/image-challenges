import net.preibisch.legacy.io.IOFunctions;
import net.preibisch.mvrecon.fiji.plugin.Define_Multi_View_Dataset;

public class Main {
	public static void main(String...args) {
// 		DebugTools.setRootLevel("INFO");
// 		StackList.defaultDirectory = args[0];
		IOFunctions.printIJLog = true;
		(new Define_Multi_View_Dataset()).run((String)null);
	}
}
