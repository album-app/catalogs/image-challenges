module.exports = {
  pathPrefix: `/catalogs/image-challenges`,
  siteMetadata: {
    title: 'Image Challenges',
    subtitle: 'A list of solutions to image challenges curated by Helmholtz Imaging',
    catalog_url: 'https://gitlab.com/album-app/catalogs/image-challenges',
    menuLinks:[
      {
         name:'Catalog',
         link:'/catalog'
      },
      {
         name:'About',
         link:'/about'
      },
    ]
  },
  plugins: [{ resolve: `gatsby-theme-album`, options: {} }],
}
